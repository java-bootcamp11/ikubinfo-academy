--Users data
INSERT INTO users (ID,NAME,SURNAME,EMAIL,BIRTHDAY,PASSWORD,ENABLED,ROLE,CREATED_AT,UPDATED_AT)
VALUES (1,'Fadil','Ademi','fadilademi@yahoo.com','1980-10-09','pass123',FALSE,'PROFESSOR','2022-10-09 17:27:23.545335','2022-10-09 17:27:23.545335'),
(2,'Azem','Murati','azemmurati@yahoo.com','1978-10-09','pass123',FALSE,'PROFESSOR','2022-10-09 17:27:23.545335','2022-10-09 17:27:23.545335'),
(3,'Ben','Cako','bencako@yahoo.com','1994-10-09','pass123',FALSE,'STUDENT','2022-10-09 17:27:23.545335','2022-10-09 17:27:23.545335'),
(4,'Adriana','Lubonja','adrianalubonja@yahoo.com','1995-10-09','pass123',FALSE,'STUDENT','2022-10-09 17:27:23.545335','2022-10-09 17:27:23.545335');


--Courses data
INSERT INTO course (ID,PROFESSOR_ID,CODE,NAME,SEMESTER_OFFERED,CATALOG_DATA,FROM_DATE,TO_DATE,CREATED_AT,UPDATED_AT)
VALUES (1,1,'CS001','Introduction to Programming','Fall','This course assumes no prior experience with programming. We do, of necessity, assume that the student is able to operate their own system to, at a minimum, install and set up the tools needed to program in at least one computer programming language.',
'2022-01-05','2022-04-05','2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(2,1,'CS002','Object Oriented Software Design','Fall','The initial practice of designing software often focuses on creating work products that capture the desired requirements and specifications of the proposed software.',
'2022-04-10','2022-08-10','2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(3,1,'CS003','JAVA','Fall','Java is a popular language. The Java Center is a Wikiversity content development project where participants create and organize learning resources about Java. The topic namespace contains pages that are for management and organization of small academic units at Wikiversity such as departments (see: Wikiversity:Topics). The AP Computer Science test uses Java for the curriculum. The AP Computer Science textbook at Wikibooks can be found below.',
'2022-09-10','2022-12-24','2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(4,1,'CS004','C++','Fall','Welcome to the C++ programming language. Whether you are not certain which language to pick or you have already decided on C++, you have come to the right place.',
'2023-01-5','2023-04-05','2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(5,1,'CS005','.NET','Fall','This is an advanced software development course. Learners should already be familiar with software development fundamentals.',
'2023-04-10','2023-08-05','2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(6,1,'CS006','Spring Framework','Fall','This is an advanced software development course. Learners should already be familiar with software development fundamentals.',
'2023-08-10','2023-12-05','2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335');

--Assessment Items data
INSERT INTO assessment_item (ID,NAME,WEIGHT,COURSE_ID,CREATED_AT,UPDATED_AT)
VALUES (1,'Exam',0.6,1,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(2,'Coursework',0.4,1,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),

(3,'Exam',0.6,2,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(4,'Coursework',0.4,2,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),

(5,'Exam',0.6,3,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(6,'Coursework',0.4,3,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),

(7,'Exam',0.6,4,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(8,'Coursework',0.4,4,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),

(9,'Exam',0.6,5,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(10,'Coursework',0.4,5,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),

(11,'Exam',0.6,6,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(12,'Coursework',0.4,6,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335');

--Enrolled Course data
INSERT INTO enrolled_course(ID,STUDENT_ID,COURSE_ID,CREATED_AT,UPDATED_AT)
VALUES (1,3,1,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(2,3,2,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(3,3,3,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(4,3,4,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(5,3,5,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(6,3,6,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(7,4,1,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(8,4,2,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(9,4,3,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(10,4,4,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(11,4,5,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(12,4,6,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335');


--Grades data
INSERT INTO grade (ID,ENROLLED_COURSE_ID,ASSESSMENT_ITEM_ID,POINTS,CREATED_AT,UPDATED_AT)
VALUES (1,1,1,75.6,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(2,1,2,70.6,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),

(3,2,3,69.6,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(4,2,4,75.6,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),

(5,3,5,55.5,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(6,3,6,60.0,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),

(7,4,7,75.6,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(8,4,8,60.6,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),

(9,5,9,90.0,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(10,5,10,100.0,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),

(11,6,11,40.6,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335'),
(12,6,12,35.6,'2022-01-03 17:27:23.545335','2022-01-03 17:27:23.545335');




