package com.ikubinfo.academy.dao;

import com.ikubinfo.academy.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class UserDao {

    @Autowired
    private EntityManager entityManager;

    public long saveUser(User user){
        entityManager.persist(user);
        entityManager.flush();
        return user.getId();
    }

    public User findUserById(Long id){
        return (User) entityManager.createNamedQuery("findUserById")
                .setParameter("id",id).getSingleResult();
    }

    public List<User> findUser(){
        return (List<User>) entityManager.createNamedQuery("findUsers").getResultList();
    }
}
