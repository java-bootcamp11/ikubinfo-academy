package com.ikubinfo.academy.dao;

import com.ikubinfo.academy.entity.AssessmentItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class AssessmentItemDao {

    @Autowired
    private EntityManager entityManager;

    public Long saveCourseWork(AssessmentItem courseWork){
        entityManager.persist(courseWork);
        return courseWork.getId();
    }

}
