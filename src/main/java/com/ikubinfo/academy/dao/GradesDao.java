package com.ikubinfo.academy.dao;

import com.ikubinfo.academy.entity.Grade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class GradesDao {

    @Autowired
    EntityManager entityManager;

    public Long saveGrade(Grade grade){
        entityManager.persist(grade);
        return grade.getId();
    }
}
