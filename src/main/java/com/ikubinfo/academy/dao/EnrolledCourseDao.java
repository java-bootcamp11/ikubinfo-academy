package com.ikubinfo.academy.dao;

import com.ikubinfo.academy.entity.EnrolledCourse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;

@Repository
public class EnrolledCourseDao {

    @Autowired
    private EntityManager entityManager;

    public Long saveEnrolledCourse(EnrolledCourse enrolledCourse){
        entityManager.persist(enrolledCourse);
        return enrolledCourse.getId();
    }
}
