package com.ikubinfo.academy.controller;

import com.ikubinfo.academy.dto.CourseDTO;
import com.ikubinfo.academy.service.CourseService;
import lombok.RequiredArgsConstructor;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequiredArgsConstructor
@Controller
@RequestMapping("/course")
public class CourseController {

    @Autowired
    private CourseService courseService;
     
    @GetMapping("/form")
    public String getCourseForm(Model model) {
    	model.addAttribute("header","Add New Course");
    	model.addAttribute("course",new CourseDTO());
    	return "courses/course-form";
    }
    
    @PostMapping
    public String saveCourse(@Valid @ModelAttribute("course") CourseDTO dto) {
    	courseService.addCourse(dto);
    	return "redirect:/course/list";
    }

    @GetMapping("/list")
    public String getCourses(Model model){
        model.addAttribute("courses",courseService.getCourses());
        return "courses/courseList";
    }
}
