package com.ikubinfo.academy.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@RequiredArgsConstructor
@Controller
@RequestMapping("/dashboard")
public class Dashboard {

    @GetMapping
    public String getIndex1(){
        return "dashboard/index";
    }
}
