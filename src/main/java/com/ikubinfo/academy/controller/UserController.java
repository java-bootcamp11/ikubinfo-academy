package com.ikubinfo.academy.controller;

import com.ikubinfo.academy.dto.UserDTO;
import com.ikubinfo.academy.entity.User;
import com.ikubinfo.academy.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@Controller
@RequestMapping("/user")
public class UserController {

    private final UserService userService;

    @GetMapping("/form")
    public String showAddUser(Model model){
        UserDTO u = new UserDTO();
        model.addAttribute("user",u);
        model.addAttribute("header","Add New User");
        return "user/userForm";
    }

    @PostMapping
    public String saveUser(@Valid @ModelAttribute("user") UserDTO userDTO, BindingResult result){
        if(result.hasErrors()){
            return "user/userForm";
        }
        userService.saveUser(userDTO);

        return "redirect:/user/list";
    }

    @GetMapping("/list")
    public String showUserList(Model model){
        List<User> users = userService.getUsers();
        model.addAttribute("users",users);
        return "user/userList";

    }


}
