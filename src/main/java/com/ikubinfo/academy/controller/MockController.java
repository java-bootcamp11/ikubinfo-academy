package com.ikubinfo.academy.controller;

import com.ikubinfo.academy.dto.Questionare;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


@Controller
public class MockController  {

    @GetMapping("/header")
    public String getIndex1(@RequestHeader(value = "User-Agent") String userAgen){
        System.err.println(userAgen);
        return "header";
    }

    @GetMapping("/login")
    public String getIndex6(HttpServletRequest request){
        System.err.println(request.getHeader("User-Agent"));
        return "login";
    }

    @GetMapping("/homepage")
    public String getIndex3(){
        return "hompage";
    }

    @GetMapping("/inputForm")
    public String getIndex4(){
        return "InputForm";
    }
    @GetMapping("/courseDetails")
    public String getIndex5(){
        return "courseDetails";
    }

    @GetMapping("/index")
    public String getIndex9(){
        return "registerForm/thank-you-page";
        
    }
    
    @GetMapping("/index1")
    public String getIndex11(Model model){
        var question = new Questionare("radio","this is question tu ask","", Arrays.asList("Answer 1","Answer 2","Answer 3"));
        model.addAttribute("question",question);
        return "index1";
    }

   @GetMapping("/profile")
    public String getIndex10(){
        return "registerForm/profile";
    }

    @GetMapping("/grid")
    public String getIndex180(){
        return "grid";
    }

    @GetMapping("/userList")
    public String getIndex181(){
        return "user/userList";
    }


}
