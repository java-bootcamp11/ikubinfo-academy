package com.ikubinfo.academy;

import com.ikubinfo.academy.dao.*;
import com.ikubinfo.academy.entity.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@EnableJpaAuditing
@EnableWebMvc
@SpringBootApplication
public class IkubinfoAcademyApplication implements CommandLineRunner {

    @Autowired
    private UserDao userDao;

    @Autowired
    private CourseDao courseDao;

    @Autowired
    private AssessmentItemDao courseWorkDao;

    @Autowired
    private EnrolledCourseDao enrolledCourseDao;

    @Autowired
    private GradesDao gradesDao;

    public static void main(String[] args) {
        SpringApplication.run(IkubinfoAcademyApplication.class, args);
    }

    @Transactional
    @Override
    public void run(String... args) throws Exception {

        User user = new User();
        user.setName("user1");
        user.setSurname("surename1");
        user.setEmail("user@email.com");
        user.setBirthday(LocalDate.now());
        user.setPassword("password");
        user.setEnabled(false);

          Long userId = userDao.saveUser(user);
          user.setId(userId);

        Course course = new Course();
        course.setCode("CS1111");
        course.setName("Computer Science");
        course.setSemesterOffered("Fall");
        course.setCatalogData("This are all catalog data");
        course.setProfessor(user);
        course.setFrom(LocalDate.now());
        course.setTo(LocalDate.now());

        Long courseId = courseDao.saveCourse(course);
        course.setId(courseId);

        AssessmentItem courseWork = new AssessmentItem();
        courseWork.setName("Course work here");
        courseWork.setWeight(0.4);
        courseWork.setCourse(course);

        Long courseworkId1 =courseWorkDao.saveCourseWork(courseWork);
        courseWork.setId(courseworkId1);

        AssessmentItem exam = new AssessmentItem();
        exam.setName("Exam");
        exam.setWeight(0.6);
        exam.setCourse(course);

        Long courseworkId2 = courseWorkDao.saveCourseWork(exam);
        exam.setId(courseworkId2);

        EnrolledCourse enrolledCourse = new EnrolledCourse();
        enrolledCourse.setStudent(user);
        enrolledCourse.setCourse(course);

        Long enrolledCourseId = enrolledCourseDao.saveEnrolledCourse(enrolledCourse);
        enrolledCourse.setId(enrolledCourseId);

        Grade gradeCourseWork = new Grade();
        gradeCourseWork.setEnrolledCourse(enrolledCourse);
        gradeCourseWork.setAssessmentItem(courseWork);
        gradeCourseWork.setPoints(64.5);

        gradesDao.saveGrade(gradeCourseWork);

        Grade gradeExam = new Grade();
        gradeExam.setEnrolledCourse(enrolledCourse);
        gradeExam.setAssessmentItem(exam);
        gradeExam.setPoints(90.5);

        gradesDao.saveGrade(gradeExam);


    }

}
