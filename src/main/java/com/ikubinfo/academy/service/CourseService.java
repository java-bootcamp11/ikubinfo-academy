package com.ikubinfo.academy.service;

import com.ikubinfo.academy.dao.CourseDao;
import com.ikubinfo.academy.dto.CourseDTO;
import com.ikubinfo.academy.entity.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
public class CourseService {

    @Autowired
    private CourseDao courseDao;

    @Transactional
    public List<Course> getCourses(){
        return courseDao.findCourses();
    }
    
    @Transactional
    public Long addCourse(CourseDTO dto) {
    	Course course = new Course();
    	course.setCode(dto.getCode());
    	course.setName(dto.getName());
    	course.setSemesterOffered(dto.getSemesterOffered());
    	course.setFrom(LocalDate.parse(dto.getFrom()));
    	course.setTo(LocalDate.parse(dto.getTo()));
    	course.setCatalogData(dto.getCatalogData());
    	
    	return courseDao.saveCourse(course);
    }
    
    
}
