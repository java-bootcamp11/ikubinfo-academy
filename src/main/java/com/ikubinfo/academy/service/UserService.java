package com.ikubinfo.academy.service;

import com.ikubinfo.academy.dao.UserDao;
import com.ikubinfo.academy.dto.UserDTO;
import com.ikubinfo.academy.entity.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@RequiredArgsConstructor
@Service
public class UserService {
    private final UserDao userDao;

    @Transactional
    public Long saveUser(UserDTO userDTO){
        User user = new User();
        user.setName(userDTO.getName());
        user.setSurname(userDTO.getSurname());
        user.setEmail(userDTO.getEmail());
        user.setPassword(userDTO.getPassword());
        user.setBirthday(LocalDate.parse(userDTO.getBirthday()));
        user.setRole(userDTO.getRole());
        user.setEnabled(true);

        return userDao.saveUser(user);
    }

    @Transactional
    public List<User> getUsers(){
        return userDao.findUser();
    }
}
