package com.ikubinfo.academy.dto;

import com.ikubinfo.academy.entity.Course;
import com.ikubinfo.academy.entity.EnrolledCourse;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    private Long id;
    @NotEmpty(message = "Name is required")
    private String name;
    @NotEmpty(message = "Surname is required")
    private String surname;
    @Email(message = "Not valid email")
    private String email;
    @NotEmpty(message = "Password is required")
    private String password;
    private String birthday;
    private Boolean enabled;
    @NotEmpty(message = "Role is required")
    private String role;
    private List<Course> courses;
    private List<EnrolledCourse> enrolledCourses;
}
