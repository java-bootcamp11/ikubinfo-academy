package com.ikubinfo.academy.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Questionare {

    String type;
    String question;
    String selectedAnswer;
    List<String> answers;
}
