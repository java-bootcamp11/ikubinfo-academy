package com.ikubinfo.academy.dto;

import java.util.List;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import com.ikubinfo.academy.entity.AssessmentItem;
import com.ikubinfo.academy.entity.EnrolledCourse;
import com.ikubinfo.academy.entity.User;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CourseDTO {
	
	private Long id;

    @Size(min = 5, message = "Min length for code is 5")
    @Size(max = 6, message = "max length for code is 5")
    @NotBlank(message = "Code is required")
    private String code;

    @NotBlank(message = "Name is required")
    private String name;

    @NotBlank(message = "Offered semester is required")
    private String semesterOffered;

    @Size(max = 500, message = "Maximum character allowed are 500")
    @NotBlank(message = "Offered semester is required")
    private String catalogData;

    private String from;

    private String to;

    private User professor;

    private List<AssessmentItem> courseWorks;

    private List<EnrolledCourse> enrolledCourses;

}
