package com.ikubinfo.academy.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

@Data
@Entity
@Table(name = "ASSESSMENT_ITEM")
public class AssessmentItem extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @NotBlank(message = "Name is required")
    @Column(name = "NAME")
    private String name;

    @NotNull(message = "Weight is required")
    @Column(name = "WEIGHT")
    private Double weight;

    @ManyToOne
    @JoinColumn(name = "COURSE_ID")
    private Course course;

    @OneToMany(mappedBy = "assessmentItem", fetch = FetchType.LAZY)
    private List<Grade> grades;

}
