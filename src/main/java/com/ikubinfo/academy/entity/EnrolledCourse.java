package com.ikubinfo.academy.entity;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@Table(name = "ENROLLED_COURSE")
public class EnrolledCourse extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;
    @ManyToOne
    @JoinColumn(name = "STUDENT_ID")
    private User student;

    @ManyToOne
    @JoinColumn(name = "COURSE_ID")
    private Course course;

    @OneToMany(mappedBy = "enrolledCourse", fetch = FetchType.LAZY)
    private List<Grade> grades;

}
