package com.ikubinfo.academy.entity;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "GRADE")
public class Grade extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "ENROLLED_COURSE_ID")
    private EnrolledCourse enrolledCourse;

    @ManyToOne
    @JoinColumn(name = "ASSESSMENT_ITEM_ID")
    private AssessmentItem assessmentItem;

    private Double points;
}
