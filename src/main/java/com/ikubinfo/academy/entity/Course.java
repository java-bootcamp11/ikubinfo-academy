package com.ikubinfo.academy.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@Table(name = "COURSE")
@NamedQueries({
        @NamedQuery(name = "findCourseById",
                query ="SELECT c FROM Course AS c WHERE c.id = :id" ),
        @NamedQuery(name = "findCourses", query = "SELECT c FROM Course AS c")
})
public class Course extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @Size(min = 5, message = "Min length for code is 5")
    @Size(max = 6, message = "max length for code is 5")
    @NotBlank(message = "Code is required")
    @Column(name = "CODE")
    private String code;

    @NotBlank(message = "Name is required")
    @Column(name = "NAME")
    private String name;

    @NotBlank(message = "Offered semester is required")
    @Column(name = "SEMESTER_OFFERED")
    private String semesterOffered;

    @Size(max = 500, message = "Maximum character allowed are 500")
    @NotBlank(message = "Offered semester is required")
    @Column(name = "CATALOG_DATA")
    private String catalogData;

    @Column(name = "FROM_DATE")
    private LocalDate from;

    @Column(name = "TO_DATE")
    private LocalDate to;

    @ManyToOne
    @JoinColumn(name = "PROFESSOR_ID")
    private User professor;

    @OneToMany(mappedBy = "course", fetch = FetchType.LAZY)
    private List<AssessmentItem> courseWorks;

    @OneToMany(mappedBy = "course", fetch = FetchType.LAZY)
    private List<EnrolledCourse> enrolledCourses;

}
