package com.ikubinfo.academy.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.List;

@Data
@Entity
@Table(name = "USERS")
@NamedQueries({
        @NamedQuery(name = "findUserById",
            query ="SELECT u FROM User AS u WHERE u.id = :id" ),
        @NamedQuery(name = "findUsers", query = "SELECT u FROM User AS u")
})
public class User extends BaseEntity{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @NotEmpty(message = "Name is required")
    @Column(name = "NAME")
    private String name;

    @NotEmpty(message = "Surname is required")
    @Column(name = "SURNAME")
    private String surname;

    @Email(message = "Not valid email")
    @Column(name = "EMAIL")
    private String email;

    @NotNull
    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "BIRTHDAY")
    private LocalDate birthday;

    @Column(name = "ENABLED")
    private Boolean enabled;

//    @NotNull
    @Column(name = "ROLE")
    private String role;

    @OneToMany(mappedBy = "professor", fetch = FetchType.LAZY)
    private List<Course> courses;

    @OneToMany(mappedBy = "student", fetch = FetchType.LAZY)
    private List<EnrolledCourse> enrolledCourses;



}
